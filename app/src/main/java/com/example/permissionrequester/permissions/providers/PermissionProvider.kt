package com.example.permissionrequester.permissions.providers

import com.example.permissionrequester.permissions.providers.text.PermissionTextProvider

interface PermissionProvider {
    val textProvider: PermissionTextProvider
    val permission: String
}