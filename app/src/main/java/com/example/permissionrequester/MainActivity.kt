package com.example.permissionrequester

import android.os.Bundle
import android.widget.Toast
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.material3.Button
import androidx.compose.material3.Text
import com.example.permissionrequester.permissions.PermissionRequester
import com.example.permissionrequester.permissions.RequestPermissionContext
import com.example.permissionrequester.permissions.providers.CoarseLocationPermissionProvider
import com.example.permissionrequester.permissions.providers.FineLocationPermissionProvider
import com.example.permissionrequester.ui.theme.PermissionRequesterTheme

class MainActivity : ComponentActivity() {

    private val permissionsToRequest = listOf(
        FineLocationPermissionProvider(),
        CoarseLocationPermissionProvider()
    )

    private lateinit var permissionRequester: PermissionRequester

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        permissionRequester = PermissionRequester(this, permissionsToRequest)

        setContent {
            PermissionRequesterTheme {
                RequestPermissionContext(permissionRequester) {
                    Button(
                        onClick = {
                            requestPermissions()
                            Toast.makeText(
                                this@MainActivity,
                                "Button clicked",
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                    ) {
                        Text(text = "Button that needs permissions")
                    }
                }
            }
        }
    }
}